let formSubmit = document.querySelector('#createCourse');

let cName = document.querySelector("#courseName");
let cPrice = document.querySelector('#coursePrice');
let cDesc = document.querySelector('#courseDescription');

let cnSpan = cName.nextElementSibling;
let cpSpan = cPrice.nextElementSibling;
let cdSpan = cDesc.nextElementSibling;

let spanSpan = cpSpan.nextElementSibling;
// console.log(spanSpan)


formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = cName.value;
	let price = cPrice.value;
	let description = cDesc.value;

	let token = localStorage.getItem("token");

	if(courseName === '' && price === '' && description === ''){
		// console.log("hi")
		cnSpan.classList.remove("d-none");
		cpSpan.classList.remove("d-none");
		cdSpan.classList.remove("d-none");

	} else if((typeof(courseName) === 'string') && (typeof(description) === 'string' )){
		fetch('https://tranquil-citadel-85121.herokuapp.com/api/courses', {

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`

			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => { 
			// console.log(data) 

			if(data === true) {

				window.location.replace('./courses.html');

			} else {

				alert("Course Creation Failed. Something went wrong.");
			}
		})
	} else {
		alert("Course Creation Failed. Something went wrong.");
	}
})