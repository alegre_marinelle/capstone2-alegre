let params = new URLSearchParams(window.location.search);
let userId = params.get("userId");

//console.log(userId)


let pwd1 = document.querySelector('#password1');
let pwd2 = document.querySelector('#password2');

let cancel = document.querySelector('#cancelChange');

cancel.innerHTML = `<a href="./profilePage.html" class="btn btn-block btn-primary my-3 submitbtn">Cancel</a>`


document.querySelector('#editPassword').addEventListener("submit", (e) => {

		e.preventDefault()

		if (pwd1.value === pwd2.value) {

			let token = localStorage.getItem('token');
			let password = pwd1.value;

			fetch('https://tranquil-citadel-85121.herokuapp.com/api/users/changePwd', {

				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
						id: userId,
						password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				//console.log(data)

				if(data === true){

					alert('Password has been edited');

					window.location.replace('./profilePage.html');

				} else {

					alert('Something went wrong.')
				}
			})
		}

		
	})

