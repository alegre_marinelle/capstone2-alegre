let params = new URLSearchParams(window.location.search);

// console.log(params.has('courseId'));
// console.log(params.get('courseId'));

let courseId = params.get('courseId');
let token = localStorage.getItem('token');
let adminUser = localStorage.getItem('isAdmin') === 'true';

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');
let enrollees = document.querySelector('#enrolleeContainer');
let enr = document.querySelector('#enrollee');

if (adminUser === false){
	fetch(`https://tranquil-citadel-85121.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		//console.log(data)
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`;

		document.querySelector('#enrollButton').addEventListener("click", () => {

			if(token){
				fetch('https://tranquil-citadel-85121.herokuapp.com/api/users/enroll', {

					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId,

					})
				})
				.then(res => res.json())
				.then(data => {
					
					if(data === true){
						alert('Thankyou for enrolling to the course!');
						window.location.replace('./courses.html');
					}else {
						// alert('Something went wrong.')
						alert("Course has already been enrolled");
						window.location.replace('./courses.html');
					}

				}).catch((err) => {
					alert("Course has already been enrolled");
					window.location.replace('./courses.html');
				})
			} else {
				window.location.replace("./register.html");
			}
		});

	});
} else {
	//console.log("Hello admin user!")

	fetch(`https://tranquil-citadel-85121.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;

		//console.log(data)

		if(data.enrollees.length === 0){

			
			// enrollees.innerHTML = `<h5 class="card-title">No Enrollees</h5>`
			enr.innerHTML = "No enrollees"


		} else {
			data.enrollees.forEach(enrollee => {
				//console.log(enrollee)
			
				let userId = enrollee.userId

				fetch(`https://tranquil-citadel-85121.herokuapp.com/api/users/details/${userId}`)
				.then(res => res.json())
				.then(data => {


					let enrolleeName = `${data.firstName} ${data.lastName}`;

					enr.innerHTML = 'Enrollees:'
					if (data) {
						enrollees.innerHTML +=
							` 
								<div class="card my-3">
									<div class="card-body">
										<h5 class="card-title">${enrolleeName}</h5>
									</div>
								</div>
							`
					} else {
						alert("Something went wrong admin");
					}
				})
			})
		}

		enrollContainer.innerHTML = `<button id="backButton" class="btn btn-block btn-primary">Back to Courses</button>`;

		document.querySelector('#enrollContainer').addEventListener("click", () =>{
				window.location.replace('./courses.html');
		})
	});
};