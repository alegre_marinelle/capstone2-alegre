//let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");
let cardFooter;

if(adminUser === false || adminUser === null){

	addButton.innerHTML = null;

	fetch('https://tranquil-citadel-85121.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		//console.log(data);

		// A variable that will store the data to be rendered
		let courseData;

		// if the number of courses is less than 1, display no courses available
		if(data.length < 1) {
			courseData = "No courses available";
		} else {
			courseData = data.map(course => {
				//console.log(course);

					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-primary text-white selectButton">Select Course
						</a>
					`;

				return(
					`	
						<div class="col-md-6 my-3">
							<div class="card h-100">
								<div class="card-body bg-light">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-right"> &#8369; ${course.price}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				);
			}).join("")
		}
		let container = document.querySelector("#coursesContainer");
		container.innerHTML = courseData;
	});

} else {
	addButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary add-course">Add Course</a>
		</div>
	`;

	fetch('https://tranquil-citadel-85121.herokuapp.com/api/course')
	.then(res => res.json())
	.then(data => {
		//console.log(data);

		let courseData;

		if(data.length < 1) {
			courseData = "No courses available";
		} else {
			courseData = data.map(course => {
				//console.log(course);

				if(course.isActive === false) {

					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-success text-white viewButton">View Course
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-primary text-white editButton">Edit Course
						</a>
						<a href="./reactivateCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-info text-white reactivateButton">Reactivate Course
						</a>
						<a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block dangerButton"> 
                            Delete Course
                        </a>
					`;

				} else {

					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-success text-white viewButton">View Course
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-primary text-white editButton">Edit Course
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-block btn-danger text-white archiveButton">Archive Course
						</a>
						<a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block dangerButton"> 
                            Delete Course
                        </a>
					`;
				}

				return(
					`
						<div class="col-md-6 my-3">
							<div class="card h-100">
								<div class="card-body bg-light">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-right"> &#8369; ${course.price}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				);
			}).join("")
		}
		let container = document.querySelector("#coursesContainer");
		container.innerHTML = courseData;
	});
}

