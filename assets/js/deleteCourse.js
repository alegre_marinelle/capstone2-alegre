/*==========
Activity 1
==========

Create a deleteCourse.js file that will store the scripts for the delete function.
Get the courseId from the URL.
Get the token of the user from the local storage.
Create a fetch request to delete the specific course.
Create a logic to redirect the user back to the courses page on success and alert the user if there's an error.

*/

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://tranquil-citadel-85121.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {
	// console.log(data)
	if(data) {
		window.location.replace('./courses.html')
	} else {
		alert("Something went wrong.")
	}
})