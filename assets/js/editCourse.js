/*==========
Activity 2
==========

Create a editCourse.js file that will store the scripts for the delete function.
Get the courseId from the URL
Select the form input fields.
Create a fetch request that will get the details of a specific course.
Assign the retrieved data values as placeholder attributes and the current values of the formSubmit.
Add an event listener to the form
Create variables that will store the details from the form and a variable to store the token of the user
Create a fetch request that will edit the course
Create a logic to redirect the user back to the courses page on success and alert the user if there's an error*/

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

let name = document.querySelector('#courseName');
let price = document.querySelector('#coursePrice');
let description = document.querySelector('#courseDescription');

fetch(`https://tranquil-citadel-85121.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data.name)

	name.placeholder = data.name;
	description.placeholder = data.description;
	price.placeholder = data.price;

	name.value = data.name;
	price.value = data.price;
	description.value = data.description

	document.querySelector('#editCourse').addEventListener('submit', (e) => {

		e.preventDefault()

		//console.log(name.value)

		let courseName;
		let desc;
		let priceVal;

		if((name.value === '') && (price.value === '') && (description.value === '')){

			courseName = data.name;
			priceVal = data.price;
			desc = data.description

		} else if(name.value === '') {

			courseName = data.name;
			priceVal = price.value;
			desc = description.value;

		} else if (price.value === '') {

			priceVal = data.price;
			courseName = name.value;
			desc = description.value;

		} else if (description.value === '') {

			desc = data.description
			courseName = name.value;
			priceVal = price.value;

		} else {

			courseName = name.value;
			priceVal = price.value;
			desc = description.value;
		}

		let token = localStorage.getItem('token');

		fetch('https://tranquil-citadel-85121.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`

			},
			body: JSON.stringify({
				id: courseId,
				name: courseName,
				description: desc,
				price: priceVal
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){
				alert('Course has been edited');
				window.location.replace('./courses.html');
			}else {
				alert('Something went wrong.')
			}

		})
	})
})

