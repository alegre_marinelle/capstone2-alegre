let params = new URLSearchParams(window.location.search);
let userId = params.get("userId");


//console.log(userId)

let fName = document.querySelector('#firstName');
let lName = document.querySelector('#lastName');
let mobileNo = document.querySelector('#mobileNumber');
let email = document.querySelector('#userEmail');

//add link / anchor tag for change password
let link =  document.querySelector('#changePass');

// console.log(firstName, lastName, mobileNo, email, pwd1, pwd2)

link.innerHTML = `<a href="./changePassword.html?userId=${userId}" value="{userId}" class="btn changePass"> Change Password </a>`

fetch(`https://tranquil-citadel-85121.herokuapp.com/api/users/details/${userId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)

	fName.placeholder = data.firstName;
	lName.placeholder = data.lastName;
	mobileNo.placeholder = data.mobileNo;
	email.placeholder = data.email;

	fName.value = data.firstName;
	lName.value = data.lastName;
	mobileNo.value = data.mobileNo;
	email.value = data.email;

	document.querySelector('#editProfile').addEventListener("submit", (e) => {
		e.preventDefault()

		let token = localStorage.getItem('token');

		let firstn = fName.value;
		let lastn = lName.value;
		let mobileNum = mobileNo.value;
		let userEmail = email.value;

		fetch('https://tranquil-citadel-85121.herokuapp.com/api/users', {
			method: "PUT",
			headers:{

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				id: userId,
				firstName: firstn,
				lastName: lastn,
				email: userEmail,
				mobileNo: mobileNum,
				// password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				alert('Profile has been edited');
				window.location.replace('./profilePage.html');
			}else {
				alert('Something went wrong.')
			}
		})
	})
})