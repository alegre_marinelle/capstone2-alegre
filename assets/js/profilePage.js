let token = localStorage.getItem('token');

let name = document.querySelector("#userName");
let desc = document.querySelector("#userDesc");
let userCourses = document.querySelector("#enrollmentsContainer");
let courseStatus = document.querySelector('#enrollmentdetails');
let editButton = document.querySelector('#editButton');
let backBtn = document.querySelector('#backBtn');
let courseArray = [];



fetch(`https://tranquil-citadel-85121.herokuapp.com/api/users/details`,{

	headers: {
		'Authorization' : `Bearer ${token}`
	}
	
})
.then(res => res.json())
.then(data =>{

	if (data) {
		editButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./editProfile.html?userId=${data._id}" value="{data._id}" class="btn btn-block btn-outline-info">Edit Profile</a>
			</div>
		`;

		name.innerHTML =` ${data.firstName}  ${data.lastName}`;
		desc.innerHTML =`Mobile Number: ${data.mobileNo}	Email: ${data.email}`;
	} else {

		alert("Something went wrong");
	}


	data.enrollments.forEach(course => {

		let courseId = course.courseId
		//console.log(course)
		
		fetch(`https://tranquil-citadel-85121.herokuapp.com/api/courses/${courseId}`,{

			headers: {
				"Content-Type": 'application/json',
				'Authorization': `Bearer ${token}`
			}
			
		})
		.then(res => res.json())
		.then(result =>{		
			//console.log(result)

			if (result) {
				userCourses.innerHTML +=
				` 
					<div class="card my-2 col-sm-6 col-md-6 col-lg-6 offset-md-3 border-success">
						<div class="card-body">
							<h5 class="card-title">${result.name}</h5>
							<h6 class="card-subtitle mb-2 text-muted">${result.description}</h6>
							<h6 class="card-subtitle mb-2 text-muted">Enrolled on: ${course.enrolledOn.toLocaleString()}</h6>
							<h6 class="card-subtitle mb-2 text-muted">Status: ${course.status}</h6>
						</div>
					</div>
				`
			} else {
				alert("Something went wrong");
			}

		})
	})
})
