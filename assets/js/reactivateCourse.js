let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://tranquil-citadel-85121.herokuapp.com/api/course/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {
	// console.log(data)
	if(data) {
		window.location.replace('./courses.html')
	} else {
		alert("Something went wrong.")
	}
})